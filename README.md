# Coding Exercise

## Introduction

The scope of this test is not only technical – it also tries to determine your approach to some of the other challenges we face as software engineers, e.g. dealing with uncertainty, asking for clarification, and agreeing upon scope. The last thing we want to do is take up an unbounded amount of your personal time trying to achieve “perfection” on the tasks outlined in the exercise.
 
With that in mind, consider the requirements in this exercise negotiable. It’s up to you to determine your priorities in completing the tasks outlined below, and how much of your time you think is reasonable to allocate to the exercise as a whole. 

## Background

The NPI Registry Public Search is a free directory of all active National Provider Identifier (NPI) records. Healthcare providers acquire their unique 10-digit NPIs to identify themselves in a standard way throughout their industry. Some basic documentation is available at:

https://npiregistry.cms.hhs.gov/registry/help-api
 
## Your challenge

Using the NPI API create a web application that:

* display an UI including a header with the title 'NPI Registry List: 08540'
* reads and displays a sortable table of providers from zip code 08540 including their:
    * NPI number
    * name
    * telephone number
    * last updated date
    * link to a card that display table's rows fields above and in additon display primary address
 
## Notes:

* The design and implementation of the application is totally up to you - you may use whichever libraries/frameworks you feel appropriate to get the job done, but be prepared to justify your choices.
* Application should have decent cross-compatibility with modern browsers.
* If you feel like this is going to take you an unreasonable amount of time to complete, please us know and we’ll figure out how to de-scope.
* Git repository to be shared when completed
